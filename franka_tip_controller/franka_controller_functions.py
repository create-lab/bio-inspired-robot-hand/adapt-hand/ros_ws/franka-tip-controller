import numpy as np
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64MultiArray

# import musicalbeeps
# beeper = musicalbeeps.Player(volume = 1,
#                             mute_output = True)
# beeper.play_note("A", 0.5)

def floatarray_to_posestamped(in_msg:Float64MultiArray):
    out_msg = PoseStamped()
    out_msg.pose.position.x = in_msg.data[0]
    out_msg.pose.position.y = in_msg.data[1]
    out_msg.pose.position.z = in_msg.data[2]

    out_msg.pose.orientation.x = in_msg.data[3]
    out_msg.pose.orientation.y = in_msg.data[4]
    out_msg.pose.orientation.z = in_msg.data[5]
    out_msg.pose.orientation.w = in_msg.data[6]

    return out_msg

def euler_from_quaternions(x, y, z, w):    
    sinr_cosp = 2 * (w * x + y * z)
    cosr_cosp = 1 - 2 * (x * x + y * y)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    sinp = 2 * (w * y - z * x)
    pitch = np.arcsin(sinp)

    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y * y + z * z)
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    return roll, pitch, yaw

def quaternions_from_euler(pitch, yaw, roll):
    cy = np.cos(yaw * 0.5)
    sy = np.sin(yaw * 0.5)
    cp = np.cos(pitch * 0.5)
    sp = np.sin(pitch * 0.5)
    cr = np.cos(roll * 0.5)
    sr = np.sin(roll * 0.5)

    # q = [w, x, y, z]
    q = [0] * 4
    q[0] = cy * cp * cr + sy * sp * sr
    q[1] = cy * cp * sr - sy * sp * cr
    q[2] = sy * cp * sr + cy * sp * cr
    q[3] = sy * cp * cr - cy * sp * sr

    return q

def franka_pose_quat_to_euler(pose:PoseStamped):
    curr_roll, curr_pitch, curr_yaw = euler_from_quaternions(
                                        pose.pose.orientation.x,
                                        pose.pose.orientation.y,
                                        pose.pose.orientation.z,
                                        pose.pose.orientation.w) 
    
    return [pose.pose.position.x, pose.pose.position.y, pose.pose.position.z, 
            curr_pitch, curr_yaw, curr_roll]

def franka_pose_euler_to_quat(franka_demand_pose:list):
    target_pose = PoseStamped()

    target_pose.pose.position.x = franka_demand_pose[0]
    target_pose.pose.position.y = franka_demand_pose[1]
    target_pose.pose.position.z = franka_demand_pose[2]

    q = quaternions_from_euler( franka_demand_pose[3], 
                                franka_demand_pose[4], 
                                franka_demand_pose[5])

    target_pose.pose.orientation.w = q[0]
    target_pose.pose.orientation.x = q[1]
    target_pose.pose.orientation.y = q[2]
    target_pose.pose.orientation.z = q[3]

    return target_pose

    