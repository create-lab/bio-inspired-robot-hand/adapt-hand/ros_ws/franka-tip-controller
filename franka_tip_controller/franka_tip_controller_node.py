import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64MultiArray
from dynamic_reconfigure.srv import Reconfigure
from dynamic_reconfigure.msg import DoubleParameter
from scipy.spatial.transform import Rotation as R
# from mocap_msgs.msg import RigidBodies
import numpy as np
from .franka_controller_functions import *

class FrankaTipControllerNode(Node):

    def __init__(self):
        super().__init__('franka_tip_controller_node')

        # Publishers
        self.franka_true_pose_publisher = self.create_publisher(PoseStamped, '/equilibrium_pose', 10)
        self.franka_current_pose_publisher = self.create_publisher(Float64MultiArray, '/franka/pose', 10)
        
        # Subscribers
        self.franka_true_pose_subscriber = self.create_subscription(PoseStamped, '/cartesian_pose', self.franka_pose_callback, 10)
        self.franka_pose_demand_subscriber = self.create_subscription(Float64MultiArray, '/franka/pose_demand', self.franka_pose_demand_callback, 10)
        self.franka_stiffness_variation = self.create_subscription(Float64MultiArray, '/franka/stiffness_demand', self.franka_stiffness_demand_callback, 10)

        # Services
        self.cli = self.create_client(Reconfigure, '/dynamic_reconfigure_compliance_param_node/set_parameters')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = Reconfigure.Request()

    # Callback when a message comes from the arm
    def franka_pose_callback(self, franka_current_pose:PoseStamped):
        self.franka_euler_pose = franka_pose_quat_to_euler(franka_current_pose)
        translation = [franka_current_pose.pose.position.x, franka_current_pose.pose.position.y, franka_current_pose.pose.position.z]
        q_array = [franka_current_pose.pose.orientation.x, franka_current_pose.pose.orientation.y, franka_current_pose.pose.orientation.z, franka_current_pose.pose.orientation.w]
        
        print_msg = "\n"
        for i, quantity in enumerate(["x", "y", "z", "qx", "qy", "qz", "qw", "pitch", "yaw", "roll"]):
            if i < 3:
                data = str(round(self.franka_euler_pose[i],3))
            elif i < 7:
                data = str(round(q_array[i-3],3))
            else:
                data = str(round(np.degrees(self.franka_euler_pose[i-4]),3))

            print_msg += quantity + ": " + data + "\n"

            if i == 2 or i == 6 or i ==9:
                print_msg += "\n"

        self.get_logger().info(print_msg, throttle_duration_sec = 0.1)

        msg = Float64MultiArray()
        msg.data = translation + q_array
        self.franka_current_pose_publisher.publish(msg)

    # Callback when a demand pose is received
    def franka_pose_demand_callback(self, demand_pose:Float64MultiArray):
        out_msg = floatarray_to_posestamped(demand_pose)
        out_msg.header.stamp = self.get_clock().now().to_msg()
        
        self.franka_true_pose_publisher.publish(out_msg)

    # Callback when a demand stiffness is received
    def franka_stiffness_demand_callback(self, franka_stiffness_demand:Float64MultiArray, current_pose_as_equilibirum = True):
        
        if current_pose_as_equilibirum:
            franka_quat_pose = franka_pose_euler_to_quat(self.franka_euler_pose)
            self.franka_true_pose_publisher.publish(franka_quat_pose)

        self.set_stiffness(franka_stiffness_demand.data)
   
    # Call the service to change the stiffness of the tip
    def set_stiffness(self, stiffness):
        stiffness_names = ["translational_stiffness_X", "translational_stiffness_Y", "translational_stiffness_Z",
                 "rotational_stiffness_X", "rotational_stiffness_Y", "rotational_stiffness_Z", "nullspace_stiffness"]
        
        msg = []
        for i, stiff in enumerate(stiffness):
            temp = DoubleParameter()
            temp.name = stiffness_names[i]
            temp.value = stiff

            msg.append(temp)

        self.req.config.doubles = msg
        self.future = self.cli.call_async(self.req)
        
        return self.future.result()

def main(args=None):
    rclpy.init(args=args)

    franka_tip_controller_node = FrankaTipControllerNode()

    rclpy.spin(franka_tip_controller_node)

    franka_tip_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()