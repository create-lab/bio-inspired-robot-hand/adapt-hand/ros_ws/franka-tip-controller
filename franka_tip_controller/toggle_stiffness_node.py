import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions

class StiffnessToggle(Node):

    def __init__(self):
        super().__init__('stiffness_toggle')
        
        self.gp = GamepadFunctions()

        # Publisher
        self.franka_stiffness_demand_publisher = self.create_publisher(Float64MultiArray, '/franka/stiffness_demand', 10)

        # Subscriber
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)

        demand_stiffness = Float64MultiArray()
        demand_stiffness.data = [400.0, 400.0, 400.0, 30.0, 30.0, 30.0, 0.0]
        self.franka_stiffness_demand_publisher.publish(demand_stiffness)

        print("Press 'right' for zero stiffness")
        print("Press 'left' for default stiffness")

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        if self.gp.if_button_pressed("right"):
            self.get_logger().info("\nSetting stiffness to zero\n", throttle_duration_sec = 0.01)

            demand_stiffness = Float64MultiArray()
            demand_stiffness.data = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            self.franka_stiffness_demand_publisher.publish(demand_stiffness)

        if self.gp.if_button_pressed("left"):
            self.get_logger().info("\nSetting stiffness back to default\n", throttle_duration_sec = 0.01)
            
            demand_stiffness = Float64MultiArray()
            demand_stiffness.data = [400.0, 400.0, 400.0, 30.0, 30.0, 30.0, 0.0]
            self.franka_stiffness_demand_publisher.publish(demand_stiffness)

def main(args=None):
    rclpy.init(args=args)

    stiffness_toggle = StiffnessToggle()

    rclpy.spin(stiffness_toggle)

    stiffness_toggle.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()